<div id="main" class="position-center">
    <div id="main-content">
        <form action="/register" method="post">
            <div class="form-input">
                <label for="name">Nome:</label><br>
                <input type="text" name="name" placeholder="nome" value="<?= Input::post('name'); ?>" required>
            </div>
            <div class="form-input">
                <label for="date">Data de Nascimento:</label><br>
                <input type="date" name="date" value="<?= Input::post('date'); ?>" required>
            </div>
            <div class="form-input">
                <label for="email">E-mail:</label><br>
                <input type="email" name="email" placeholder="e-mail" value="<?= Input::post('email'); ?>" required>
            </div>
            <div class="form-input">
                <label for="pass">Senha:</label><br>
                <input type="password" name="pass" placeholder="senha" value="<?= Input::post('pass'); ?>" required>
            </div>

            <button type="submit" name="send" class="btn btn-full">Cadastrar</button>
            <a href="/login" class="btn-link btn-full">Volar</a>
        </form>
    </div>
    <div id="main-notifications">
        <?php if(isset($response['msg'])):?>
            <div class="notifications-content <?= $response['status'] ? 'notifications-success' : 'notifications-danger'; ?>">
                <?= $response['msg']; ?>
            </div>
        <?php endif; ?>
    </div>
</div>