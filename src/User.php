<?php

class User {
    private $db;

    public function __construct()
    {
        $this->db = new Database();
        $this->db->table('user');
    }

    public function save($name,$email,$pass,$date) {
        $response['status'] = true;
        $response['msg'] = '';

        // VALIDATE DATA INPUTS
        if(!filter_var($name,FILTER_SANITIZE_STRING)) {
            $response['status'] = false;
            $response['msg'] = " O nome de usuario `{$name}` não é valido!";
            goto end;
        }
        if(!filter_var($email,FILTER_SANITIZE_EMAIL)) {
            $response['status'] = false;
            $response['msg'] = " O endereço de e-mail `{$email}` não é valido!";
            goto end;
        }
        if(!$this->validate_date($date)) {
            $response['status'] = false;
            $response['msg'] = " A data de nascimento {$date} não é valido!";
            goto end;
        }

        // check email in use
        if($this->db->count_result(['email' => $email])) {
            $response['status'] = false;
            $response['msg'] = "O endereço de email `{$email}` já está em uso!";
            goto end;
        }

        $data['name'] = $name;
        $data['email'] = $email;
        $data['pass'] = password_hash($pass,PASSWORD_DEFAULT);
        $data['date'] = $date;

        if($this->db->insert($data))
            $response['msg'] = 'Usuário cadastrado com sucesso!';
        else {
            $response['status'] = false;
            $response['msg'] = 'Não foi possivel cadastrar o usário no momento :(';
        }

        end:
        return $response;
    }

    public function remove($id) {
        $response['status'] = true;
        $response['msg'] = '';

        if(!$this->db->count_result(['id'=> $id])) {
            $response['status'] = false;
            $response['msg'] = 'Error: não foi possível encontrar o usuário!';
            goto end;
        }

        if($this->db->remove(['id' => $id]))
            $response['msg'] = 'Usuário removido com sucesso!';
        else {
            $response['status'] = false;
            $response['msg'] = 'Não foi possível remover o usuário no momento :(';
        }

        end:
        return $response;
    }

    public function change_password($id, $passCurrent, $passNew) {
        $response['status'] = true;
        $response['msg'] = '';

        if(!$this->db->count_result(['id'=> $id])) {
            $response['status'] = false;
            $response['msg'] = 'Error: não foi possível encontrar o usuário!';
            goto end;
        }

        $passHash = $this->db->get_first(['id' => $id],'pass')['pass'];
        if(!password_verify($passCurrent,$passHash)) {
            $response['status'] = false;
            $response['msg'] = 'Error: senha atual inválida!';
            goto end;
        }

        $passNew = password_hash($passNew,PASSWORD_DEFAULT);
        if($this->db->update(['pass'=> $passNew], ['id' => $id]))
            $response['msg'] = 'Senha de usuário alterada com sucesso!';
        else {
            $response['status'] = false;
            $response['msg'] = 'Não foi possível alterar a senha do  usuário no momento :(';
        }

        end:
        return $response;
    }

    public function authentication($email,$pass) {
        $response['status'] = true;
        $response['msg'] = '';

        $user = $this->db->get_first(['email' => $email]);
        if(empty($user)) {
            $response['status'] = false;
            $response['msg'] = "O endereço de email `{$email}` não está vinculado a nenhum usuário!";
            goto end;
        }

        if(password_verify($pass, $user['pass'])) {
            Session::save($user['id'],$user['name']);
            $response['msg'] = "Autenticação efetuada com sucesso!";
        }
        else {
            $response['status'] = false;
            $response['msg'] = "Senha de usuário inválida!";
        }

        end:
        return $response;
    }

    private function validate_date($date, $format = 'Y-m-d'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}