<?php require __DIR__ . "/dashboard.php"; ?>

<div id="main" class="position-center">
    <div id="main-content">
        <form action="/change_password" method="post">
            <div class="form-input">
                <label for="passCurrent">Senha Atual:</label><br>
                <input type="password" name="passCurrent" placeholder="senha atual" required><br>
            </div>

            <div class="form-input">
                <label for="passNew">Nova Senha:</label><br>
                <input type="password" name="passNew" placeholder="nova senha" required><br>
            </div>
            <button type="submit" name="send" class="btn btn-full">Enter</button>
        </form>
    </div>
    <div id="main-notifications">
        <?php if(isset($response['msg'])):?>
            <div class="notifications-content <?= $response['status'] ? 'notifications-success' : 'notifications-danger'; ?>">
                <?= $response['msg']; ?>
            </div>
        <?php endif; ?>
    </div>
</div>