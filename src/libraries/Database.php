<?php

class Database {
    private $db;
    private $table;

    public function __construct()
    {
        try {
            $db['driver'] = 'mysql';
            $db['host'] = 'db';
            $db['name'] = 'wlib';
            $db['user'] = 'root';
            $db['pass'] = 'toor';
            $this->db = new \PDO("{$db['driver']}:host={$db['host']};dbname={$db['name']}", $db['user'], $db['pass']);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->table = null;

        } catch(PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    public function __destruct()
    {
        try {
            $this->db = null;
        } catch (PDOException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    public function table($name) {
        $this->table = $name;

        return $this;
    }

    public function get(Array $wheres = null, $select = null, $rows = null) {

        // prepare select from result
        $select = !empty($select) ? $select : '*';

        // prepare fields wheres query
        if(!empty($wheres)) {
            $wheres = implode(', ', array_map(function($value,$field){
                return "{$field}='{$value}'";
            },$wheres, array_keys($wheres)));

           $wheres =  'WHERE '. $wheres;
        }
        else
            $wheres = '';

        // prepare limit result
        $limit = !empty($rows) ? "LIMIT {$rows}" : '';

        // prepare string sql
        $sql = "select {$select} from {$this->table} {$wheres} {$limit}";
        $query = $this->db->query($sql);
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function get_first(Array $wheres = null, $select = null) {
        $result = $this->get($wheres,$select,1);
        return !empty($result) ? $result[0] : false;
    }

    public function insert(Array $fields) {
        // get fields name
        $_fields = implode(", ", array_keys($fields));
        $_values = implode(", ", array_map(function($value) {
                    return ":{$value}";
                  }, array_keys($fields)));

        // prepare string sql
        $sql = "INSERT INTO {$this->table}({$_fields}) VALUES({$_values})";
        $stmt = $this->db->prepare($sql);

        // set bind statement from pdo
        foreach ($fields as $field => $value)
          $stmt->bindValue(":{$field}", $value, PDO::PARAM_STR);

        if($stmt->execute())
            return $this->db->lastInsertId();

        return false;
    }

    public function update(Array $fields, Array $wheres) {
        // prepare set fields
        $_fields = implode(', ', array_map(function($field){
            return "{$field}=:{$field}";
        }, array_keys($fields)));
        $_fields = "SET {$_fields}";

        // prepare fields wheres query
        $_wheres = implode(', ', array_map(function($field){
            return "{$field}=:{$field}";
        },array_keys($wheres)));
        $_wheres =  'WHERE '. $_wheres;

        // prepare string sql
        $sql = "UPDATE {$this->table} {$_fields} {$_wheres}";
        $stmt = $this->db->prepare($sql);

        // set bind statement from pdo
        foreach ($fields as $field => $value)
            $stmt->bindValue(":{$field}", $value, PDO::PARAM_STR);
        foreach ($wheres as $field => $value)
            $stmt->bindValue(":{$field}", $value, PDO::PARAM_STR);

        if($stmt->execute())
            return $stmt->rowCount();

        return false;
    }

    public function remove(Array $wheres) {
        // prepare fields wheres query
        $_wheres = implode(', ', array_map(function($field){
            return "{$field}=:{$field}";
        },array_keys($wheres)));

        $_wheres =  'WHERE '. $_wheres;

        // prepare string sql
        $sql = "DELETE FROM {$this->table} {$_wheres}";
        $stmt = $this->db->prepare($sql);

        //set bind statement from pdo
        foreach ($wheres as $field => $value)
            $stmt->bindValue(":{$field}", $value, PDO::PARAM_STR);

        if($stmt->execute())
            return $stmt->rowCount();

        return false;
    }

    public function count_result(Array $wheres = null) {
        // prepare fields wheres query
        $wheres = implode(', ', array_map(function($value,$field){
            return "{$field}='{$value}'";
        },$wheres, array_keys($wheres)));
        $wheres =  'WHERE '. $wheres;

        // prepare string sql
        $sql = "select * from {$this->table} {$wheres}";
        $stmt = $this->db->prepare($sql);

        $stmt->execute();
        return $stmt->rowCount();
    }
}