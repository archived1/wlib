<?php

function base_url($path = NULL)
{
    $protocolo = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://';
    $urlBase   = $protocolo . $_SERVER['HTTP_HOST'];
    $urlBase  .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
    $url       = $urlBase.$path;

    return $url;
}

function redirect($url) {
    header("Location:" . $url);
    exit();
}

function render_view($name, $data = array()) {
    $pathView = __DIR__ . "/views/{$name}.php";
    $pathTemplate = __DIR__ . "/views/_template.php";

    if (is_readable($pathView)) {

        foreach ($data as $key => $value)
            $$key = $value;

        ob_start();
        include $pathView;
        $content = ob_get_contents();
        ob_end_clean();

        include $pathTemplate;
        exit();
    }
    else
        throw new Error("View `{$name}` not found!");
}

