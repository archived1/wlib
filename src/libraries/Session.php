<?php

class Session {

    public static function has_session() {
        if(isset($_SESSION['logged']))
            return true;

        return false;
    }

    public static function required_logged() {
        if(!self::has_session())
            redirect(base_url('login'));
    }

    public static function is_logged() {
        if(self::has_session())
            redirect(base_url('dashboard'));
    }

    public static function set_flashdata($name,$data) {
        $_SESSION['flashdata'][$name] = $data;
    }

    public static function flashdata($name) {
        $flashdata = isset($_SESSION['flashdata'][$name]) ? $_SESSION['flashdata'][$name] : null;

        if($flashdata)
            unset($_SESSION['flashdata'][$name]);

        return $flashdata;
    }

    public static function get() {
        return $_SESSION['user'];
    }

    public static function save($userid,$username) {
        $_SESSION['logged'] = true;
        $_SESSION['user'] = ['userid' => $userid, 'username' => $username];

        return true;
    }

    public static function close() {
        $_SESSION = array();
        session_destroy();
    }
}