<nav id="nav-top">
    <h1><a href="/dashboard">Dashboard</a></h1>
    <div class="position-center">
        <?= $user['username']; ?>
    </div>
    <ul>
        <li>
            <a href="<?= (Route::get_route() == '/change_password') ? '#' : '/change_password'; ?>">Alterar Senha</a>
        </li>
        <li><a href="/logout">Sair</a></li>
    </ul>
</nav>

