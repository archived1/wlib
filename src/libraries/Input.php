<?php
class Input {

    public static function post($name = null, $default = null) {
        return empty($name) ? $_POST : (isset($_POST[$name]) ? $_POST[$name] : $default);
    }

    public static function get($name = null, $default = null) {
        return empty($name) ? $_GET : (isset($_GET[$name]) ? $_GET[$name] : $default);
    }

    public static function has($name) {
        return isset($_REQUEST[$name]) ? true : false;
    }
}