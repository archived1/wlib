<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title; ?></title>
    <style>
        * {
            padding: 0px;
            margin: 0px;
        }
        html,body {
            width: 100%;
            height: 100%;
            background: #ECF0F1;
            font-family: Arial, Helvetica, sans-serif;
        }

        #main {
            width: max-content;
            height: max-content;
            box-sizing: content-box;
        }
        #main > #main-content {
            background: #fff;
            padding: 20px;
            border-radius: 6px;
            box-shadow: 0px 0px 4px rgba(0,0,0,.08);
        }

        #main > #main-notifications {
            padding-top: 30px;
        }
        .notifications-content {
            padding: 5px;
            border-radius: 4px;
            text-align: center;
        }
        .notifications-content.notifications-danger {
            background: #FFAC9C;
            color: #B71C0C;
        }
        .notifications-content.notifications-success {
            background: #8EFFC1;
            color: #007C21;
        }

        #nav-top {
            width: 100%;
            height: 60px;
            background: #fff;
            box-shadow: 1px 0px 4px rgba(0,0,0,.1);
            position: relative;
        }
        #nav-top * {
            display: inline-block;
            text-decoration: none;
            color: #212121;
        }
        #nav-top h1 > a{
            height: 100%;
            padding: 15px  30px;
            box-sizing: border-box;
        }
        #nav-top > div {
            display: table;
            width: max-content;
            margin: auto;
        }
        #nav-top ul {
            height: 100%;
            list-style: none;
            float: right;

        }
        #nav-top ul > li {
            height: 100%;
            float: left;
        }
        #nav-top ul > li > a{
            width: 100% ;
            height: 100%;
            padding: 20px 30px;
            box-sizing: border-box;
            transition: background .2s ;
        }
        #nav-top ul > li > a:hover {
            background: rgba(236, 240, 241,.4);
            box-shadow: inset 0px 0px 3px rgba(0,0,0,.2);
        }







        .position-center {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            position: absolute;
        }

        .form-input {
            margin-bottom: 15px;
        }
        .form-input label{
            padding-left: 5px;
            font-weight: bolder;
            font-size: 0.75rem;
            color: #727272;
            text-transform: lowercase;
        }
        .form-input input {
            width: 100%;
            margin-top: 5px;
            padding: 10px;
            border: none;
            box-sizing: border-box;
            box-shadow: inset 0px 0px 4px rgba(0,0,0,.1);
        }

        .btn {
            display: table;
            background: #ecf0f1;
            margin: auto;
            margin-top: 5px;
            padding: 10px 20px;
            font-size: 0.9rem;
            color: #212121;
            text-transform: uppercase;
            text-decoration: none;
            text-align: center;
            border: none;
        }
        .btn-full {
            width: 100%;
        }
        a.btn-link, a.btn-link:visited, a.btn-link:active {
            display: table;
            margin-top: 5px;
            padding: 10px 0px;
            color: #727272;
            text-align: center;
            text-decoration: none;
            transition: text-decoration .8s ease;
        }
        a.btn-link:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <?= $content ?>

</body>
</html>