<?php
class Route {
    private static $routes = array();

    public static function get($route,$callback, $params = array()) {
        self::$routes[$route] = ['method' => 'GET', 'callback' => $callback, 'params' => $params];
    }

    public static function post($route,$callback, $params = array()) {
        self::$routes[$route] = ['method' => 'POST', 'callback' => $callback, 'params' => $params];
    }

    public static function all($route,$callback, $params = array()) {
        self::$routes[$route] = ['method' => 'ALL', 'callback' => $callback, 'params' => $params];
    }

    public static function run($route) {
        // check route is registred
        if(!array_key_exists($route,self::$routes))
            self::not_found();

        // check method
        if((self::$routes[$route]['method'] != 'ALL') && ($_SERVER['REQUEST_METHOD'] != self::$routes[$route]['method']))
            self::not_found();

        // run callback
        call_user_func(self::$routes[$route]['callback'], self::$routes[$route]['params']);
        exit;
    }

    public static function start() {
        $route = self::get_route();
        self::run($route);
    }

    public static function get_route()
    {
        // get path url
        $pathUrl = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        // remove index.php
        $route = str_replace('index.php/', NULL, $pathUrl);

        return $route;
    }

    public static function not_found(){
        $data['title'] = 'Error 404';
        render_view('error404');
        exit;
    }
}