<div id="main" class="position-center">
    <div id="main-content">
        <form action="/login" method="post">
            <div class="form-input">
                <label for="email">E-mail:</label><br>
                <input type="email" name="email"j id="email" placeholder="e-mail" required>
            </div>
            <div class="form-input">
                <label for="pass">Senha:</label><br>
                <input type="password" name="pass"  id="pass" placeholder="senha" required><br>
            </div>
            <button type="submit" name="send" class="btn btn-full">Enter</button>
            <a href="/register" class="btn-link btn-full">Inscreva-se!</a>
        </form>
    </div>
    <div id="main-notifications">
        <?php if(isset($response['msg'])):?>
            <div class="notifications-content <?= $response['status'] ? 'notifications-success' : 'notifications-danger'; ?>">
                <?= $response['msg']; ?>
            </div>
        <?php endif; ?>
    </div>
</div>