<?php
//  set charset to UTF8
ini_set('default_charset', 'UTF-8');
header('Content-Type: text/html; charset=UTF-8');

// set environment
error_reporting(E_ALL);
ini_set('display_errors',1);

session_start();

// load base;
require __DIR__ . '/src/helpers.php';
require __DIR__ . '/src/libraries/Session.php';
require __DIR__ . '/src/libraries/Route.php';
require __DIR__ . '/src/libraries/Database.php';
require __DIR__ . '/src/libraries/Input.php';
require __DIR__ . '/src/User.php' ;

Route::get('/', function (){
    if(Session::has_session())
        redirect(base_url('dashboard'));
    else
        redirect(base_url('login'));
});
Route::get('/dashboard', function (){
    Session::required_logged();

    $data['title'] = 'Dashboard';
    $data['response'] = Session::flashdata('response');
    $data['user']  = Session::get();
    render_view('dashboard', $data);
});
Route::all('/login', function (){
    Session::is_logged();

    // get request post
    if(Input::has('send')) {
        $email = Input::post('email');
        $pass = Input::post('pass');
        $user = new User();
        $response = $user->authentication($email,$pass);
        Session::set_flashdata('response',$response);

        if($response['status'])
            redirect('dashboard');
    }

    $data['title'] = 'Login';
    $data['response'] = Session::flashdata('response');
    render_view('login', $data);
});
Route::all('/logout', function (){
    Session::close();
    redirect('login');
});
Route::all('/register', function (){
    Session::is_logged();

    if(Input::has('send')) {
        $name = Input::post('name');
        $email = Input::post('email');
        $pass = Input::post('pass');
        $date = Input::post('date');
        $user = new User();
        $response = $user->save($name,$email,$pass,$date);

        if($response['status']) {
            Session::set_flashdata('response', $response);
            redirect('login');
        }


        $data['response'] = $response;
    }

    $data['title'] = 'Registre-se';
    render_view('register', $data);
});
Route::all('/change_password', function (){
    Session::required_logged();

    // get request post
    if(Input::has('send')) {
        $userid = Session::get()['userid'];
        $passCurrent = Input::post('passCurrent');
        $passNew = Input::post('passNew');
        $user = new User();
        $response = $user->change_password($userid,$passCurrent, $passNew);
        Session::set_flashdata('response',$response);

        if($response['status'])
            redirect('dashboard');
    }

    $data['title'] = 'Dashboard';
    $data['response'] = Session::flashdata('response');
    $data['user']  = Session::get();

    render_view('change_password', $data);
});

Route::start();